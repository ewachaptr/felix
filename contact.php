<?php
/*
Template Name: Contact
*/
?>

<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <header class="turquoise">
    <?php $group = get_field('contact_info', 'option'); ?>
    <section>
      <h1><?php the_title(); ?></h1>
      <h2>Address</h2>
      <p><?php echo $group['address']; ?></p>

      <ul>
        <li>
          <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/telephone.svg">
          <span><?php echo $group['phone_number']; ?></span>
        </li>
        <li>
          <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/email.svg">
          <div>
            <span><?php echo $group['email_address']; ?></span>
            <p>(please see email guidelines below)</p>
          </div>
        </li>
        <?php $socialHandles = get_field('social_handles', 'option'); ?>
        <li>
          <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/twitter2.svg">
          <span><?php echo $socialHandles['twitter_handle']; ?></span>
        </li>
        <li>
          <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/instagram.svg">
          <span><?php echo $socialHandles['twitter_handle']; ?></span>
        </li>
      </ul>
    </section>
    <section id="map">
    		<?php
    		$location = get_field('map');

    		if($location):
    		?>
    		<div class="acf-map">
    			<div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
    		</div>
    		<?php endif; ?>

    <?php endwhile; endif; ?>

    <style type="text/css">

    .acf-map {
    	width: 100%;
    	height: 100%;
    	border: #ccc solid 1px;
    	margin: 20px 0;
    }

    /* fixes potential theme css conflict */
    .acf-map img {
       max-width: inherit !important;
    }

    </style>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDgXLsqt8LwABxbXZO0EPgV8bpowF_hoHA"></script>
    <script type="text/javascript">
    (function($) {

    /*
    *  new_map
    *
    *  This function will render a Google Map onto the selected jQuery element
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$el (jQuery element)
    *  @return	n/a
    */

    function new_map( $el ) {

    	// var
    	var $markers = $el.find('.marker');


    	// vars
    	var args = {
    		zoom		: 16,
    		center		: new google.maps.LatLng(0, 0),
    		mapTypeId	: google.maps.MapTypeId.ROADMAP
    	};


    	// create map
    	var map = new google.maps.Map( $el[0], args);


    	// add a markers reference
    	map.markers = [];


    	// add markers
    	$markers.each(function(){

        	add_marker( $(this), map );

    	});


    	// center map
    	center_map( map );


    	// return
    	return map;

    }

    /*
    *  add_marker
    *
    *  This function will add a marker to the selected Google Map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	$marker (jQuery element)
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function add_marker( $marker, map ) {

      // var
      var latlng = new google.maps.LatLng( $marker.attr('data-lat'), $marker.attr('data-lng') );

      // create marker
      var marker = new google.maps.Marker({
        position	: latlng,
        map			: map
      });

      // add to array
      map.markers.push( marker );

      // if marker contains HTML, add it to an infoWindow
      if( $marker.html() )
      {
        // create info window
        var infowindow = new google.maps.InfoWindow({
          content		: $marker.html()
        });

        // show info window when marker is clicked
        google.maps.event.addListener(marker, 'click', function() {

          infowindow.open( map, marker );

        });
      }

    }

    /*
    *  center_map
    *
    *  This function will center the map, showing all markers attached to this map
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	4.3.0
    *
    *  @param	map (Google Map object)
    *  @return	n/a
    */

    function center_map( map ) {

    	// vars
    	var bounds = new google.maps.LatLngBounds();

    	// loop through all markers and create bounds
    	$.each( map.markers, function( i, marker ){

    		var latlng = new google.maps.LatLng( marker.position.lat(), marker.position.lng() );

    		bounds.extend( latlng );

    	});

    	// only 1 marker?
    	if( map.markers.length == 1 )
    	{
    		// set center of map
    	    map.setCenter( bounds.getCenter() );
    	    map.setZoom( 16 );
    	}
    	else
    	{
    		// fit to bounds
    		map.fitBounds( bounds );
    	}

    }

    /*
    *  document ready
    *
    *  This function will render each map when the document is ready (page has loaded)
    *
    *  @type	function
    *  @date	8/11/2013
    *  @since	5.0.0
    *
    *  @param	n/a
    *  @return	n/a
    */
    // global var
    var map = null;

    $(document).ready(function(){

    	$('.acf-map').each(function(){

    		// create map
    		map = new_map( $(this) );

    	});

    });

    })(jQuery);
    </script>

    </section>
  </header>

<?php $contact = get_field('contact', 5);
if( $contact ): ?>
<article id="contact" class="green">
  <section>
    <p><?php echo $contact['paragraph']; ?></p>
    <?php
      if( have_rows('contact', 5) ): while ( have_rows('contact', 5) ) : the_row();
        if( have_rows('more_paragraphs') ):
          while ( have_rows('more_paragraphs') ) : the_row(); ?>
              <h2><?php the_sub_field('title'); ?></h2>
              <p><?php the_sub_field('paragraph'); ?></p>
          <?php endwhile; endif;
      endwhile; endif;
    ?>
  </section>
  <section>
    <?php gravity_form('Home', false, false, false, '', false); ?>
  </section>
</article>
<?php endif; ?>





<?php get_footer(); ?>
