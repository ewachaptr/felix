<?php get_header(); ?>
<?php if ( have_posts() ) :
  while ( have_posts() ) : the_post();
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
  $url = $thumb['0'];
  ?>
  <header class="turquoise">
    <h1><?php the_title(); ?></h1>
    <div>
      <span><?php echo get_the_date( 'F Y' ); ?></span> / <span>Author: <?php echo get_the_author(); ?></span>
    </div>
    <aside>
      <?php $group = get_field('contact_info', 'option'); ?>
      <?php $socialLinks = get_field('social_links', 'option'); ?>
      <ul>
        <li><a href="<?php echo $socialLinks['twitter_link']; ?>"><img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/twitter2.svg"></a></li>
        <li><a href="<?php echo $socialLinks['instagram_link']; ?>"><img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/instagram.svg"></a></li>
      </ul>
    </aside>
  </header>
  <section id="primary" class="full section content-area os-animation" data-os-animation="fadeInUp">
    <div>
      <article id="post-<?php the_ID(); ?>">
        <div class="images">
          <?php if ($url): ?>
            <img class="b-lazy" 
            src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
            data-src="<?php echo $url; ?>">
          <?php endif ?>

          <?php $image = get_field('image2'); ?>
          <?php if (!empty($image)): ?>
            <img class="b-lazy" 
            src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
            data-src="<?php echo $image['sizes']['large'] ?>">
            <?php endif ?>
          
          <?php $image = get_field('image3'); ?>
          <?php if (!empty($image)): ?>
            <img class="b-lazy" 
            src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
            data-src="<?php echo $image['sizes']['large'] ?>">
          <?php endif ?>

        </div>
        <div class="entry-content">
          <?php the_content(); ?>
          <div id="highlight"><?php the_field('highlight') ?></div>
          <?php $button = get_field('button'); ?>
          <?php if (!empty($button)): ?>
            <div class="last"><a href="<?= $button ?>" class="btn">Read original article</a></div>
          <?php endif; ?>
        </div>
      </article>
    </div>
  </section>
  <?php endwhile; endif; ?>
<?php get_footer(); ?>