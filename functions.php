<?php

/*********************
WP_HEAD CLEANUP
*********************/

function head_cleanup() {
  // EditURI link
  remove_action( 'wp_head', 'rsd_link' );
  // windows live writer
  remove_action( 'wp_head', 'wlwmanifest_link' );
  // previous link
  remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 );
  // start link
  remove_action( 'wp_head', 'start_post_rel_link', 10, 0 );
  // links for adjacent posts
  remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 );
  // WP version
  remove_action( 'wp_head', 'wp_generator' );
  // remove WP version from css
  add_filter( 'style_loader_src', 'remove_wp_ver_css_js', 9999 );
  // remove Wp version from scripts
  add_filter( 'script_loader_src', 'remove_wp_ver_css_js', 9999 );

} /* end head cleanup */

// A better title
// http://www.deluxeblogtips.com/2012/03/better-title-meta-tag.html
function rw_title( $title, $sep, $seplocation ) {
  global $page, $paged;

  // Don't affect in feeds.
  if ( is_feed() ) return $title;

  // Add the blog's name
  if ( 'right' == $seplocation ) {
    $title .= get_bloginfo( 'name' );
  } else {
    $title = get_bloginfo( 'name' ) . $title;
  }

  // Add the blog description for the home/front page.
  $site_description = get_bloginfo( 'description', 'display' );

  if ( $site_description && ( is_home() || is_front_page() ) ) {
    $title .= " {$sep} {$site_description}";
  }

  // Add a page number if necessary:
  if ( $paged >= 2 || $page >= 2 ) {
    $title .= " {$sep} " . sprintf( __( 'Page %s', 'dbt' ), max( $paged, $page ) );
  }

  return $title;

} // end better title

// remove WP version from RSS
function rss_version() { return ''; }

// remove WP version from scripts
function remove_wp_ver_css_js( $src ) {
  if ( strpos( $src, 'ver=' ) )
    $src = remove_query_arg( 'ver', $src );
  return $src;
}

// remove injected CSS for recent comments widget
function remove_wp_widget_recent_comments_style() {
  if ( has_filter( 'wp_head', 'wp_widget_recent_comments_style' ) ) {
    remove_filter( 'wp_head', 'wp_widget_recent_comments_style' );
  }
}

// remove injected CSS from recent comments widget
function remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action( 'wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style') );
  }
}

// remove injected CSS from gallery
function gallery_style($css) {
  return preg_replace( "!<style type='text/css'>(.*?)</style>!s", '', $css );
}

/*********************
SCRIPTS & ENQUEUEING
*********************/

// loading modernizr and jquery, and reply script
function scripts_and_styles() {

  global $wp_styles; // call global $wp_styles variable to add conditional wrapper around ie stylesheet the WordPress way

  if (!is_admin()) {

    // modernizr (without media query polyfill)
    wp_register_script( 'modernizr', get_stylesheet_directory_uri() . '/library/js/libs/modernizr.custom.min.js', array(), '2.5.3', false );

    // blazy
    wp_register_script( 'blazy', get_stylesheet_directory_uri() . '/library/js/blazy.min.js', array('jquery'), '', true );

    // instafeed
    wp_register_script( 'instafeed', get_stylesheet_directory_uri() . '/library/js/instafeed.min.js', array(), '', true );

    // register main stylesheet
    wp_register_style( 'stylesheet', get_stylesheet_directory_uri() . '/library/css/style.css', array(), '', 'all' );

     // register font awesome stylesheet
    wp_register_style( 'font-awesome', get_stylesheet_directory_uri() . '/library/font-awesome-4.7.0/css/font-awesome.min.css', array(), '', 'all' );

    // register lightbox stylesheet
    wp_register_style( 'lightbox-stylesheet', get_stylesheet_directory_uri() . '/library/js/lightcase-2.3.4/src/css/lightcase.css', array(), '', 'all' );

    // register lightbox js
    wp_register_script( 'lightbox', get_stylesheet_directory_uri() . '/library/js/lightcase-2.3.4/src/js/lightcase.js', array('jquery'), '', true );

    // register waypoints js
    wp_register_script( 'waypoints', get_stylesheet_directory_uri() . '/library/js/jquery.waypoints.min.js', array('jquery'), '', true );

    // register blog posts scripts
    wp_register_script( 'blog-posts-scripts', get_stylesheet_directory_uri() . '/library/js/blog-posts-scripts.js', array( 'jquery', 'lightbox' ), '', true );

    // comment reply script for threaded comments
    if ( is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
      wp_enqueue_script( 'comment-reply' );
    }

    //adding scripts file in the footer
    wp_register_script( 'js', get_stylesheet_directory_uri() . '/library/js/scripts.js', array( 'jquery' ), '', true );

    // enqueue styles and scripts

    wp_enqueue_style( 'stylesheet' );

    wp_enqueue_script( 'js' );
    wp_enqueue_script( 'waypoints' );
    wp_enqueue_script( 'blazy' );
    wp_enqueue_script( 'instafeed' );
    // wp_enqueue_style( 'font-awesome' );
    // wp_enqueue_style( 'lightbox-stylesheet' );
    // wp_enqueue_script( 'lightbox' );

  }
  if (is_single()) {
    wp_enqueue_script( 'blog-posts-scripts' );
  }
}

/**************
THEME SUPPORT
***************/

function theme_support() {
  add_image_size( 'gram', 300, 300 );

  // wp thumbnails
  add_theme_support( 'post-thumbnails' );

  // rss thingy
  add_theme_support('automatic-feed-links');

  // wp menus
  add_theme_support( 'menus' );

  // registering wp3+ menus
  register_nav_menus(
    array(
      'global-menu' => __( 'Global Menu', '' ),   // main nav in header
      'footer-menu' => __( 'Footer Menu', '' )    // secondary nav in footer
    )
  );

  // Enable support for HTML5 markup.
  add_theme_support( 'html5', array(
    'comment-list',
    'search-form',
    'comment-form'
  ) );

} /* end theme support */


/*********************
CLEANUP ITEMS
*********************/

// remove the p from around imgs (http://css-tricks.com/snippets/wordpress/remove-paragraph-tags-from-around-images/)
function filter_ptags_on_images($content){
  return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
}

function excerpt_more($more) {
  global $post;
  return '...';
}

/*********************
RELATED POSTS FUNCTION
*********************/

// Related Posts Function (call using related_posts(); )
// Pass a value if you want to show more/less than 3
// If by_tags == false will display posts withing same categories, else it will display posts within same tags
function related_posts($number_posts = 3, $by_tags = false) {
  echo '<ul id="related-posts">';

  global $post;

  $tags = wp_get_post_tags( $post->ID );
  $cats = wp_get_post_categories( $post->ID );

  $tag_arr = "";
  $cat_arr = "";

  if($cats && $by_tags == false) {

    $args = array(
      'category__in' => $cats,
      'numberposts' => $number_posts, /* you can change this to show more */
      'post__not_in' => array($post->ID)
    );

    $related_posts = get_posts( $args );

    if($related_posts) {

      foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>

        <li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>

      <?php endforeach; }

    else { ?>

      <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'theme' ) . '</li>'; ?>

    <?php }

  } else if($tags && $by_tags == true) {

    foreach( $tags as $tag ) {

      $tag_arr .= $tag->slug . ',';

    }

    $args = array(
      'tag' => $tag_arr,
      'numberposts' => $number_posts, /* you can change this to show more */
      'post__not_in' => array($post->ID)
    );

    $related_posts = get_posts( $args );

    if($related_posts) {

      foreach ( $related_posts as $post ) : setup_postdata( $post ); ?>

        <li class="related_post"><a class="entry-unrelated" href="<?php the_permalink() ?>" title="<?php the_title_attribute(); ?>"><?php the_title(); ?></a></li>

      <?php endforeach; }

    else { ?>

      <?php echo '<li class="no_related_post">' . __( 'No Related Posts Yet!', 'theme' ) . '</li>'; ?>

    <?php }
  }
  wp_reset_postdata();
  echo '</ul>';
} /* end related posts function */

/*********************
PAGE NAVI
*********************/

// Numeric Page Navi (built into the theme by default)
function page_navi($the_query) {
  if (!isset($the_query)) {
    global $wp_query;
    $the_query = $wp_query;
  }

  $bignum = 999999999;
  if ( $the_query->max_num_pages <= 1 )
    return;
  echo '<nav class="pagination">';
  echo paginate_links( array(
    'base'         => str_replace( $bignum, '%#%', esc_url( get_pagenum_link($bignum) ) ),
    'format'       => '',
    'current'      => max( 1, get_query_var('paged') ),
    'total'        => $the_query->max_num_pages,
    'prev_text'    => '&larr;',
    'next_text'    => '&rarr;',
    'type'         => 'list',
    'end_size'     => 3,
    'mid_size'     => 3
  ) );
  echo '</nav>';
} /* end page navi */

/***************
LAUNCH THEME
***************/

function the_theme_setup() {
  // language support
  load_theme_textdomain( get_template_directory() . '/library/translation' );

  // A better title
  add_filter( 'wp_title', 'rw_title', 10, 3 );
  // cleanup
  add_action( 'init', 'head_cleanup' );

  // remove WP version from RSS
  add_filter( 'the_generator', 'rss_version' );
  // remove pesky injected css for recent comments widget
  add_filter( 'wp_head', 'remove_wp_widget_recent_comments_style', 1 );
  // clean up comment styles in the head
  add_action( 'wp_head', 'remove_recent_comments_style', 1 );
  // clean up gallery output in wp
  add_filter( 'gallery_style', 'gallery_style' );

  // enqueue base scripts and styles
  add_action( 'wp_enqueue_scripts', 'scripts_and_styles', 999 );

  theme_support();

  // cleaning up random code around images
  add_filter( 'the_content', 'filter_ptags_on_images' );
  // cleaning up excerpt
  add_filter( 'excerpt_more', 'excerpt_more' );

} /* end theme setup */

add_action( 'after_setup_theme', 'the_theme_setup' );

/************* OEMBED SIZE OPTIONS *************/

if ( ! isset( $content_width ) ) {
  $content_width = 680;
}

/*************************************
GRAVITY FORM FIX
**************************************/

add_filter('deprecated_constructor_trigger_error', '__return_false');

/*************************************
CUSTOM CONTENT FUNCTIONS
**************************************/

// Change excerpt length
function custom_excerpt_length( $length ) {
  return 40;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Add options page for ACF
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

// add lightbox to post images
// add_filter('the_content', 'addlightboxrel_replace', 12);
// function addlightboxrel_replace ($content) {
//   global $post;
//   $pattern = "/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
//   $replacement = '<a$1data-rel="lightcase:myCollection" href=$2$3.$4$5$6</a>';
//   $content = preg_replace($pattern, $replacement, $content);
//   $content = str_replace("%LIGHTID%", $post->ID, $content);
//   return $content;
// }

add_filter('the_content', 'my_addlightboxrel');
function my_addlightboxrel($content) {
       global $post;
       $pattern ="/<a(.*?)href=('|\")(.*?).(bmp|gif|jpeg|jpg|png)('|\")(.*?)>/i";
       $replacement = '<a$1href=$2$3.$4$5 data-rel="lightcase:myCollection" title="'.$post->post_title.'"$6>';
       $content = preg_replace($pattern, $replacement, $content);
       return $content;
}

// SVG upload support
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  $mimes['ogg'] = 'application/ogg';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 *
 * Filter Yoast SEO Metabox Priority
 * @author Jacob Wise
 * @link http://swellfire.com/code/filter-yoast-seo-metabox-priority
 *
 */
add_filter( 'wpseo_metabox_prio', 'jw_filter_yoast_seo_metabox' );
function jw_filter_yoast_seo_metabox() {
	return 'low';
}

// Hide admin bar
add_filter('show_admin_bar', '__return_false');

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyC5g5ucy4LDIeHggIAyjHXwzEAyUA15Om8');
}

add_action('acf/init', 'my_acf_init');


function my_acf_google_map_api( $api ){

	$api['key'] = 'AIzaSyDgXLsqt8LwABxbXZO0EPgV8bpowF_hoHA';

	return $api;

}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');
?>
