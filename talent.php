<?php
/*
Template Name: Talent
*/
?>

<?php get_header(); ?>

<header class="grey">
  <h1><?php the_title(); ?></h1>
  <?php include get_theme_file_path( '/searchform.php' ); ?>
</header>
<aside><a href="<?php echo get_home_url(); ?>/creatives">Looking for Creatives?</a></aside>

<?php $random_rows = get_field( 'profiles' );  ?>
<?php $counter = 1; ?>
<?php if ( is_array( $random_rows ) )  { ?>
  <?php shuffle( $random_rows ); ?> 
  <ul>
  <?php foreach ($random_rows as $random_row ) { ?>
    <?php $image = $random_row['image']; ?>
    <li class="grid-layout">
      <a href="https://spotlight.com/<?php echo $random_row['pin'];?>" target="_blank">
        <div class="image-wrapper" style="background-image: url('<?php echo $image['sizes']['medium']; ?>');" data-os-animation="fadeIn">
          <div class="overlay">
            <span class="img"><img class="b-lazy os-animation" data-os-animation="fadeIn" src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== data-src="<?php echo get_template_directory_uri();?>/library/images/spotlight.png"></span>
            <strong><?php echo $random_row['name'];?></strong>
          </div>
        </div>
      </a>
    </li>
    <?php
    $quote = get_field('the_quote', 'option');
    if (($counter === 3) || (($counter - 3) % 21 === 0))  {
      echo "<aside class=\"turquoise\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
    } elseif (($counter === 8) || (($counter - 8) % 21 === 0)) {
      echo "<aside class=\"salmon\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
    } elseif (($counter === 16) || (($counter - 16) % 21 === 0)) {
      echo "<aside class=\"green\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
    }
    $counter++; ?>   
  <?php } ?>
  </ul>
<?php } ?>

<?php get_footer(); ?>