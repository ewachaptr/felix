<?php get_header(); ?>
<?php if ( have_posts() ) :
  while ( have_posts() ) : the_post();
    $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
    $url = $thumb['0'];
  ?>

<section id="primary" class="full section content-area" class="salmon">
	<div>
      <article id="post-<?php the_ID(); ?>" class="os-animation" data-os-animation="fadeInUp">
        <div>
          <?php if ($url): ?>
            <a href="<?php echo get_home_url(); ?>/creatives" class="btn">Back to Creatives</a><br><br>
            <img class="b-lazy profile" src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw== data-src="<?php echo $url; ?>">
          <?php endif ?>
          <?php if( have_rows('keys') ):
            echo '<ul>';
            while ( have_rows('keys') ) : the_row(); ?>
              <li>
                <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/arrow.svg">
                <span><?php the_sub_field('info'); ?></span>
              </li>
            <?php endwhile;
            echo '</ul>';
          endif; ?>
        </div>
        <div class="entry-content salmon">
          <h1><?php the_title(); ?></h1>
          <?php
          if( get_field('representation')): ?>
            <p><?php the_field('representation'); ?></p>
            <br>
          <?php endif ?>
          <?php the_content(); ?>
          <?php
            $bio = get_field('bio');
            if( $bio ): ?>
              <a href="<?php echo $bio['url']; ?>" class="btn" target="_blank">Download Biography</a>
          <?php endif; ?>
        </div>
      </article>
    <?php endwhile;
		endif; ?>
	</div>
</section>
<?php get_footer(); ?>