jQuery(window).load(function() {
  jQuery(function($) {
    var bLazy = new Blazy({
      success: function(ele) {
        Waypoint.refreshAll();
      }
    });

    function onScrollInit(items, trigger) {
      items.each(function() {
        var osElement = jQuery(this),
          osAnimationClass = osElement.attr("data-os-animation"),
          osAnimationDelay = osElement.attr("data-os-animation-delay");

        osElement.css({
          "-webkit-animation-delay": osAnimationDelay,
          "-moz-animation-delay": osAnimationDelay,
          "animation-delay": osAnimationDelay
        });

        var osTrigger = trigger ? trigger : osElement;

        osTrigger.waypoint(
          function() {
            osElement.addClass("animated").addClass(osAnimationClass);
          },
          {
            offset: "90%"
          }
        );
      });
    }

    onScrollInit($(".os-animation"));

    $(".global-menu img").click(function() {
      $(this)
        .parent()
        .toggleClass("active");
    });

    var feed = new Instafeed({
      get: "user",
      userId: "8150439478",
      accessToken: "8150439478.1677ed0.3cce5d777648423dbaa06e0bb2b5307b",
      target: "instagram-inner",
      limit: 6,
      resolution: "standard_resolution",
      template:
        '<li><a href="{{link}}" class="image-wrapper" target="_blank" style="background-image: url({{image}});"></a></li>'
    });
    feed.run();
  });
});
