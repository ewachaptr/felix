<input type="text" id="search"
placeholder="search <?php $title = the_title('','',false); echo strtolower($title); ?>"></input>

<script>
jQuery(window).load(function() {
  jQuery(function($) {

    $("#search").on("keyup", function() {
        var value = $(this).val();
        value = value.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        });

        if(value!='') {
        $("main > ul > *").hide();
        } else {
        $("main > ul > *").show();
    }
    $('ul li strong:contains("'+value+'")').parent().parent().parent().parent().show(); 
    });

  });
});
</script>