<?php get_header(); ?>

<section id="primary">

test
	<?php if ( have_posts() ) :

		// Page title
		the_archive_title( '<h1 class="page-title">', '</h1>' );

		while ( have_posts() ) : the_post();

		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		$url = $thumb['0'];
	?>
		<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
			<?php // Post thumbnail. ?>
			<?php if ($url): ?>
				<img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo $url; ?>">
			<?php endif ?>

			<header class="entry-header">
				<?php the_title(  sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
			</header> <!-- .entry-header -->

			<div class="entry-content">
				<?php the_excerpt(); ?>
			</div><!-- .entry-content -->


		</article> <!-- #post-## -->

		<?php endwhile;
	endif; ?>

</section>


<?php get_footer(); ?>
