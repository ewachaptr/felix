<?php
/*
 Template Name: Blog
*/
?>

<?php get_header(); ?>
<header class="grey">
  <h1><?php the_title(); ?></h1>
</header>
<section id="primary" class="full section blog-content">
<ul>
	<?php
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	$args = array( 'post_type' => 'post', 'post_status' => 'publish', 'paged' => $paged, 'posts_per_page' => get_option('posts_per_page') );
	$loop = new WP_Query( $args );
	if ( $loop->have_posts() ) :
		while ( $loop->have_posts() ) : $loop->the_post();
		$thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'large' );
		$url = $thumb['0'];
	?>

    <?php if ($url): ?>
    <?php $big = get_field('big_image', $post->ID); ?>
    <li class="grid-layout<?php if ( $big == 1 ): ?> big<?php endif; ?>">
      <a href="<?php echo get_permalink( $post->ID ); ?>">
        <div class="image-wrapper b-lazy os-animation" data-os-animation="fadeIn" data-src="<?php echo $url; ?>">
          <div class="overlay">
            <?php if ( $big == 1 ): ?><div><?php endif; ?>
              <strong><?php the_title(); ?></strong>
              <span>Read</span>
            <?php if ( $big == 1 ): ?></div><?php endif; ?>
            <?php $page_paragraph = get_field('news_page_paragraph', $post->ID); ?>
            <?php if (( $big == 1 ) && (!empty($page_paragraph))): ?><p><?php echo $page_paragraph; ?></p><?php endif; ?>
          </div>
        </div>
      </a>
    </li>
    <?php endif ?>
		<?php endwhile;
	endif; ?>
 </ul>
</section>

<?php wp_reset_query(); ?>

<?php get_footer(); ?>