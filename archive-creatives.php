<?php
/*
Template Name: Creatives
*/
?>

<?php get_header(); ?>
<header class="grey">
  <h1><?php the_title(); ?></h1>
  <?php include get_theme_file_path( '/searchform.php' ); ?>
  
</header>
<aside><a href="<?php echo get_home_url(); ?>/talent">Looking for Talent?</a></aside>

<ul>
<?php
  $args = array( 'post_type' => 'profile', 'posts_per_page' => 1000, 'orderby' => 'rand' );
  $loop = new WP_Query( $args );
  $counter = 1;
  while ( $loop->have_posts() ) : $loop->the_post();
  $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'medium');
  $url = $thumb['0']; ?>

  <?php if ($url): ?>
    <li class="grid-layout">
      <a href="<?php echo get_permalink( $post->ID ); ?>">
        <div class="image-wrapper" style="background-image: url('<?php echo $url; ?>');" data-os-animation="fadeIn">
          <div class="overlay">
            <span>Profile</span>
            <strong><?php echo get_the_title(); ?></strong>
          </div>
        </div>
      </a>
    </li>
  <?php endif ?>

  <?php
  $quote = get_field('the_quote', 'option');
  if (($counter === 3) || (($counter - 3) % 21 === 0))  {
    echo "<aside class=\"turquoise\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
  } elseif (($counter === 8) || (($counter - 8) % 21 === 0)) {
    echo "<aside class=\"salmon\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
  } elseif (($counter === 16) || (($counter - 16) % 21 === 0)) {
    echo "<aside class=\"green\" data-os-animation=\"fadeIn\"><p>\"" . $quote . "\"</p></aside>";
  } ?>

  <?php $counter++; ?>
<?php endwhile; ?>
</ul>
<?php get_footer(); ?>