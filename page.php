<?php get_header(); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<section class="full section">
		<div class="wrap cf">
			<?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
			<?php the_content();?>
		</div>
	</section>
	
<?php endwhile; endif; ?>

<?php get_footer(); ?>