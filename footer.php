  	<?php wp_footer(); ?>
    </main>
    <footer>
      <?php $group = get_field('contact_info', 'option'); ?>
      <?php $socialLinks = get_field('social_links', 'option'); ?>
      <section>
        <ul>
          <li>
            <a href="<?php echo $socialLinks['twitter_link']; ?>">
              <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/twitter.svg">
            </a>
          </li>
          <li>
            <a href="<?php echo $socialLinks['instagram_link']; ?>">
              <img class="b-lazy" 
         src=data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==
         data-src="<?php echo get_template_directory_uri();?>/library/images/insta.svg">
            </a>
          </li>
        </ul>
      </section>
      <section>
        <ul>
          <li><?php echo $group['address']; ?></li>
          <li><?php echo $group['phone_number']; ?></li>
          <li><a href="mailto:<?php echo $group['email_address']; ?>"><?php echo $group['email_address']; ?></a></li>
        </ul>
      </section>
    </footer>
	</body>
</html>
